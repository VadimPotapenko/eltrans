<?php
include_once ('src/crest.php');
$filter = array('>=ID' => '851', '<=ID' => '976', 'CATEGORY_ID' => '6');
$template_id = '966';

$dealsTotal = CRest::call('crm.deal.list', array('select' => array('ID'), 'filter' => $filter));
$iteration = intval($dealsTotal['total'] / 50) + 1;
if ($dealsTotal['total'] % 50 == 0) $iteration -= 1;
for ($i = 0; $i < $iteration; $i++) {
	$start = $i * 50;
	$dealsData[] = array(
		'method' => 'crm.deal.list',
		'params' => array(
			'filter' => $filter,
			'select' => array('ID'),
			'start' => $start
		)
	);
}
if(count($dealsData) > 50) $dealsData = array_chunk($dealsData, 50);
else $dealsData = array($dealsData);
for ($i = 0, $s = count($dealsData); $i < $s; $i++) {
	$deals[] = CRest::callBatch($dealsData[$i]);
}

for ($i = 0, $size = count($deals); $i < $size; $i++) {
	for ($x = 0, $s = count($deals[$i]['result']['result']); $x < $s; $x++) {
		for ($y = 0, $z = count($deals[$i]['result']['result'][$x]); $y < $z; $y++) {
			$bizProcData[] = array(
				'method' => 'bizproc.workflow.start',
				'params' => array(
					'TEMPLATE_ID' => $template_id,
					'DOCUMENT_ID' => array('crm', 'CCrmDocumentDeal', $deals[$i]['result']['result'][$x][$y]['ID'])
				)
			);
		}
	}
}
if (count($bizProcData) > 50) $bizProcData = array_chunk($bizProcData, 50);
else $bizProcData = array($bizProcData);
for ($i = 0, $s = count($bizProcData); $i < $s; $i++) {
	$result[] = CRest::callBatch($bizProcData[$i]);
}

echo '<pre>';
print_r ($result);
echo '</pre>';